# webcopier
its proof of concept based on https://demoboost.com/ app

## how to run
### 1. install node & npm
```
sudo apt install nodejs
sudo apt install npm
```

### 2. download and run app
```
git clone https://gitlab.com/Marathay/webcopier
cd webcopier
cd public
mkdir scrappedData
cd ..
npm install
PORT=80 npm run start
```
it should start server on http://localhost:80/ default port 3000

## troubleshoot
### missing dependencies on WSL2/ubuntu
```
sudo apt install chromium-browser libxcursor1 libxss1
sudo apt install libatk-bridge2.0-0 libgtk-3-0
```

### workaround for running app on port 80 without extended permissions
redirect 80 -> 8080 using iptables
https://serverfault.com/questions/112795/how-to-run-a-server-on-port-80-as-a-normal-user-on-linux
```
iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 8080
```