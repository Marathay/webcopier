const scrape = require('website-scraper');
const PuppeteerPlugin = require('website-scraper-puppeteer');
const path = require('path');
const fs = require('fs');
var rimraf = require("rimraf");

const scrapePage = async function (url = 'https://simsvip.com/') {
    const dirName = url
        .replace('://www.', '://')
        .replace('https://', '')
        .replace('http://', '')
        .replace(/\//g, '_') // replacement for replaceAll("/", '_')
        .replace('ą', 'a').replace('Ą', 'A')
        .replace('ć', 'c').replace('Ć', 'C')
        .replace('ę', 'e').replace('Ę', 'E')
        .replace('ł', 'l').replace('Ł', 'L')
        .replace('ń', 'n').replace('Ń', 'N')
        .replace('ó', 'o').replace('Ó', 'O')
        .replace('ś', 's').replace('Ś', 'S')
        .replace('ż', 'z').replace('Ż', 'Z')
        .replace('ź', 'z').replace('Ź', 'Z')

    const saveDirectory = path.resolve(__dirname, '../public/' + dirName);
    try {
        rimraf.sync(saveDirectory);
    } catch(_) {
        console.log('no need to remove')
    }

    console.log(dirName)

    const results = await scrape({
        // Provide the URL(s) of the website(s) that you want to clone
        // In this example, you can clone the Our Code World website
        urls: url,
        // Specify the path where the content should be saved
        // In this case, in the current directory inside the ourcodeworld dir
        directory: saveDirectory,
        // Load the Puppeteer plugin
        plugins: [
            new PuppeteerPlugin({
                launchOptions: {
                    // If you set  this to true, the headless browser will show up on screen
                    headless: true
                }, /* optional */
                scrollToBottom: {
                    timeout: 10000,
                    viewportN: 10
                } /* optional */
            })
        ]
    }).then(function () {
        console.log("pobralem "+dirName);
    });

    return results;
};

module.exports = scrapePage