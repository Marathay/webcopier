var express = require('express');
var router = express.Router();
var scrapePage = require('../src/index');
const fs = require('fs');
const isValidHttpUrl = require("../src/isValidHttpUrl");

/* GET home page. */
router.get('/', function(req, res, next) {
  const error = req.query.error
  const success = req.query.success
  console.log(error);
  const dirs = fs.readdirSync(__dirname+'/../public/').filter(function (el) {
      return el.includes('.')
  });
  res.render('index', { title: 'ScrappApp', pages: dirs, error: error, success: success });
});

router.post('/scrape', async function(req, res, next) {
  const url = req.body.url

  if(!isValidHttpUrl(url)) {
    res.redirect('/?error=invalidurl');
  }

  await scrapePage(url);
  res.redirect('/');
});

module.exports = router;
